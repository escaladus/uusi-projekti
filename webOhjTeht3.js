/*Tehtävä: Urheilu 1

Denis Eskelinen

Määrittele yliluokka Henkilo, joka sisältää ihmisen henkilötietoja:

etunimet,
sukunimi,
kutsumanimi,
syntymävuosi
Määrittele luokka Urheilija, joka perii Henkilo-luokan ja toteuttaa lisäksi saantifunktiot (get- ja set-) Urheilija-luokalle merkityksellisiin attribuutteihin. 
Lisää Urheilija luokkaan seuraavat ominaisuudet:

linkki kuvaan,
omapaino,
laji,
saavutukset.
Kirjoita nämä vaatimukset toteuttava koodi joka toimii node.js-tulkissa.

Toteuta koodi. Lisää koodiin esimerkkejä Urheilija –olioista.
Talleta toteutuksesi gitlab (tai github) -ympäristöön omaan projektiisi hakemistoon: Urheilu1
Kirjoita tehtävästä kommentointi ja huomioita kehittäjän blogiin (hackmd.io).*/

"use strict";

class Henkilo {
  constructor(etunimet, sukunimi, kutsumanimi, syntymavuosi) {
    this._etunimet = etunimet;
    this._sukunimi = sukunimi;
    this._kutsumanimi = kutsumanimi;
    this._syntymavuosi = syntymavuosi;// = new Date(); // new syntymavuosi(Date);
  }
  get etunimet() {
    return this._etunimet;
  }
  get sukunimi() {
    return this._sukunimi;
  }
  get kutsumanimi() {
    return this._kutsumanimi;
  }
  get syntymavuosi() {
    return this._syntymavuosi;
  }
  set etunimet(uusiNimi) {
    this._etunimet = uusiNimi;
  }
  set sukunimi(uusiSukunimi) {
    this._sukunimi = uusiSukunimi;
  }
  set kutsumanimi(uusiKutsumanimi) {
    this._kutsumanimi = uusiKutsumanimi;
  }
  set syntymavuosi(uusiSyntymavuosi) {
    this._syntymavuosi = uusiSyntymavuosi;
  }
}

class Urheilija extends Henkilo {
  constructor(
    etunimet,
    sukunimi,
    kutsumanimi,
    syntymavuosi,
    linkkiKuvaan,
    omapaino,
    laji,
    saavutukset
  ) {
    super(etunimet, sukunimi, kutsumanimi, syntymavuosi);
    this._linkkiKuvaan = linkkiKuvaan;
    this._omapaino = omapaino;
    this._laji = laji;
    this._saavutukset = saavutukset;
  }
  get linkkiKuvaan() {
    return this._linkkiKuvaan;
  }
  get omapaino() {
    return this._omapaino;
  }
  get laji() {
    return this._laji;
  }
  get saavutukset() {
    return this._saavutukset;
  }
  set linkkiKuvaan(uusiLinkkikuva) {
    this._linkkiKuvaan = uusiLinkkikuva;
  }
  set omapaino(uusiOmaPaino) {
    this._omapaino = uusiOmaPaino;
  }
  set laji(uusiLaji) {
    this._laji = uusiLaji;
  }
  set saavutukset(uusiSaavutus) {
    this._saavutukset = uusiSaavutus;
  }
  getPackage(){
    console.log(this._etunimet +" " + this._kutsumanimi + " syntymävuosi " + this._syntymavuosi);
  }
  
}
var syntymavuosi = new Date();

const ihminen1 = new Urheilija("Denis","Eskelinen","Den", 1980,"http:Chhhs.com",82,"nyrkeily","SM");

ihminen1.getPackage();


console.log(ihminen1.syntymavuosi);
console.log(typeof syntymavuosi);